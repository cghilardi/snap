require 'rails_helper'

describe Video do

  let(:video_name) { "video_1" }
  let(:video_url) { "http://blah.com" }
  subject(:video) { Video.new(name: video_name, url: video_url) }

  it { is_expected.to be_valid }

  describe "attribute url" do
    it "returns the url" do
      expect(video.url).to eq(video_url)
    end

    it "is mandatory" do
      video.url = ""
      expect(video).to be_invalid
    end
  end

  describe "attribute name" do
    it "returns the name" do
      expect(video.name).to eq(video_name)
    end

    it "is mandatory" do
      video.name = ""
      expect(video).to be_invalid
      #expect(video.errors[:name].size).to eq(1)
    end
  end

end
